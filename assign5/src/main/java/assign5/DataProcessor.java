package assign5;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class DataProcessor {
	List<MonitoredData> dataList;

	public DataProcessor() {
		dataList = new ArrayList<>();
		readData();
	}

	public void readData() {
		Path path = Paths.get(System.getProperty("user.dir") + "\\src\\main\\java\\assign5\\file.txt");
		try {
			dataList = Files.lines(path).map(line -> line.split("		"))
					.collect(Collectors.mapping(s -> new MonitoredData(s[0], s[1], s[2]), Collectors.toList()));
			System.out.println("----Collected DataSet----");
			dataList.stream().forEach(data -> System.out
					.println(data.getStartTime() + " " + data.getEndTime() + " " + data.getActivity()));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public int countDays() {
		int count = dataList.stream().collect(Collectors.groupingBy(MonitoredData::getStartDate)).size();
		System.out.println("\n\n----Recorded DAYS : " + count + " ----");
		return count;
	}

	public Map<String, Integer> mapActivities(boolean print) {
		Map<String, Integer> map = dataList.stream().collect(
				Collectors.groupingBy(MonitoredData::getActivity, Collectors.reducing(0, e -> 1, Integer::sum)));
		if (print) {
			System.out.println("\n\n----Each Activity appears a number of ----");
			map.entrySet().stream().forEach(
					s -> System.out.println(s.getKey().replaceAll("\\s+", "") + " " + s.getValue() + " times"));
		}
		return map;
	}

	public Map<LocalDate, Map<String, Long>> countForDays() {
		Map<LocalDate, Map<String, Long>> map = dataList.stream().collect(Collectors.groupingBy(
				MonitoredData::getStartDate, Collectors.groupingBy(MonitoredData::getActivity, Collectors.counting())));
		System.out.println("\n\n----Per day, Each Activity appears a number of ----");
		map.entrySet().stream().forEach(System.out::println);
		return map;
	}

	public Map<String, Long> computeDurationForEachDay() {
		Map<String, Long> map = dataList.stream().collect(Collectors.toMap(MonitoredData::getActivity,
				MonitoredData::getDuration, (oldValue, newValue) -> oldValue + newValue));
		System.out.println("\n\n----For each activity the total duration is----");
		map.entrySet().stream()
				.forEach(s -> System.out.println(s.getKey().replaceAll("\\s+", "") + " " + s.getValue() + " seconds"));
		return map;
	}

	public Map<String, Long> computeEntireDuration() {
		Map<String, Long> map = dataList.stream().collect(Collectors.toMap(MonitoredData::getStartTime,MonitoredData::getDuration));
		System.out.println("\n\n----For each entry, each activity has the following duration----");
		map.entrySet().stream().sorted(Map.Entry.comparingByKey()).forEach(System.out::println);
		return map;
	}

	public List<String> findAverageMonitoredData() {
		Map<String, Integer> mapRequested = dataList.stream().filter(data -> data.getDuration() < 300).collect(
				Collectors.groupingBy(MonitoredData::getActivity, Collectors.reducing(0, e -> 1, Integer::sum)));
		Map<String, Integer> mapAll = mapActivities(false);
		System.out
				.println("\n\n----The activities that have 90% of the records with duration less than 5 minutes----");
		mapRequested.entrySet().stream().filter(data -> 100 * data.getValue() / mapAll.get(data.getKey()) >= 90)
				.forEach(System.out::println);
		return null;
	}

	public static void main(String[] args) {
		DataProcessor processor = new DataProcessor();
		processor.countDays();
		processor.mapActivities(true);
		processor.countForDays();
		processor.computeDurationForEachDay();
		processor.computeEntireDuration();
		processor.findAverageMonitoredData();
	}
}
