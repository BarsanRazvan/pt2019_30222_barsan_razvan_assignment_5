package assign5;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

public class MonitoredData {
	private String startTime;
	private String endTime;
	private String activity;
	
	public MonitoredData() {
	}
	
	public MonitoredData(String startTime, String endTime, String activity) {
		this.startTime = startTime;
		this.endTime = endTime;
		this.activity = activity;
	}
	
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public String getActivity() {
		return activity;
	}
	public void setActivity(String activity) {
		this.activity = activity;
	}
	
	public LocalDate getStartDate() {
		return LocalDate.parse(startTime.substring(0,10));
	}
	
	public LocalDate getEndDate() {
		return LocalDate.parse(endTime.substring(0,10));
	}
	
	public LocalDateTime getStartDateTime() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		return LocalDateTime.parse(startTime, formatter);
	}
	
	public LocalDateTime getEndDateTime() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		return LocalDateTime.parse(endTime,formatter);
	}
	
	public long getDayOfTheYear() {
		return LocalDate.parse(endTime.substring(0,10)).getDayOfYear();
	}
	
	public long getDuration() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		return LocalDateTime.parse(startTime,formatter).until(LocalDateTime.parse(endTime,formatter), ChronoUnit.SECONDS);
	}
	
	@Override
	public String toString() {
		return "startTime=" + startTime + ", endTime=" + endTime + ", activity=" + activity + "\n";
	}
}
